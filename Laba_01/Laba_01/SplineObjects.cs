﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laba_01
{
    public class SplineTurple
    {
        public double x;

        public double n { get; set; }
        public double a { get; set; }
        public double b { get; set; }
        public double c { get; set; }
        public double d { get; set; }

        public double Of(double x)
        {
            double dx = x - this.x;
            return a + (b + (c / 2 + (d / 6) * dx) * dx) * dx;
        }

        public double Diff1(double x)
        {
            double dx = x - this.x;
            return b + (c + (d / 2) * dx) * dx;
        }

        public double Diff2(double x)
        {
            double dx = x - this.x;
            return c + d * dx;
        }
    }

    public class SplineError
    {
        public int n { get; set; }
        public double Xn { get; set; }
        public double Xn1 { get; set; }
        public double FuncErr { get; set; }
        public double DiffErr { get; set; }
    }
}
