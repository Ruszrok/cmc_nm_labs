﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Laba_01
{
    [TestFixture]
    public class NumericOperationTest
    {
        [Test]
        public void RibbonSolveTest()
        {
            var top = new double[2] { 1, 1 };
            var bot = new double[2] { 1, 1 };
            var mid = new double[3] { 4, 4, 6 };
            var right = new double[3] { 6, 14, 32 };

            var x = NumericOperation.RibbonSolve(top, mid, bot, right);

            Assert.AreEqual(3, x.Length);
            Assert.AreEqual(1, x[0]);
            Assert.AreEqual(2, x[1]);
            Assert.AreEqual(5, x[2]);
        }
    }
}
