﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laba_01
{
    public class TestFunction : Function
    {
        private static double func(double x)
        {
            return x < 0 ? x * (x * (x + 3)) : -x * (x * (x - 3));
        }

        public override double RealDiff1(double x)
        {
            return x < 0 ? x * (3 * x + 6) : -x * (3 * x - 6);
        }

        public override double RealDiff2(double x)
        {
            return x < 0 ? 6 * x + 6 : -6 * x + 6;
        }

        public TestFunction()
            : base(func)
        {
            LeftBound = -1;
            RightBound = 1;
        }
    }

    public class MyFunction : Function
    {
        private static double func(double x)
        {
            return Math.Sqrt(1 + 3 * x * x);
        }

        public override double RealDiff1(double x)
        {
            return 3 * x / Math.Sqrt(1 + 3 * x * x);
        }

        public override double RealDiff2(double x)
        {
            return 3 / (Math.Sqrt(1 + 3 * x * x) * (1 + 3 * x * x));
        }

        public MyFunction()
            : base(func)
        {
            LeftBound = 0;
            RightBound = 2;
        }
    }

    public class MyFunctionCos10x : Function
    {
        private static double func(double x)
        {
            return Math.Sqrt(1 + 3 * x * x) + Math.Cos(10 * x);
        }

        public override double RealDiff1(double x)
        {
            return 3 * x / Math.Sqrt(1 + 3 * x * x) - 10 * Math.Sin(10 * x);
        }

        public override double RealDiff2(double x)
        {
            return 3 / (Math.Sqrt(1 + 3 * x * x) * (1 + 3 * x * x)) - 100 * Math.Cos(10 * x);
        }

        public MyFunctionCos10x()
            : base(func)
        {
            LeftBound = 0;
            RightBound = 2;
        }
    }

    public class MyFunctionCos100x : Function
    {
        private static double func(double x)
        {
            return Math.Sqrt(1 + 3 * x * x) + Math.Cos(100 * x);
        }

        public override double RealDiff1(double x)
        {
            return 3 * x / Math.Sqrt(1 + 3 * x * x) - 100 * Math.Sin(100 * x);
        }

        public override double RealDiff2(double x)
        {
            return 3 / (Math.Sqrt(1 + 3 * x * x) * (1 + 3 * x * x)) - 10000 * Math.Cos(100 * x);
        }

        public MyFunctionCos100x()
            : base(func)
        {
            LeftBound = 0;
            RightBound = 2;
        }
    }
}
