using System;

namespace Laba_01
{
    public static class NumericOperation
    {
        /// <summary>
        /// This method resolve square linear systeml like
        /// top[i]*X[i-1] + mid[i]*X[i] + bot[i]*X[i+1] = right[i]
        /// top[0] = 0, bot[n] = 0
        /// </summary>
        /// <param name="top">Elements higher than main diag</param>
        /// <param name="mid">Elements on main diag</param>
        /// <param name="bot">Elements under main diag</param>
        /// <param name="right">Values of rows</param>
        /// <returns> Solutions of system</returns>
        public static double[] RibbonSolve(double[] top, double[] mid, double[] bot, double[] right)
        {
            int n = mid.Length;
            double[] res = new double[n];

            var alfa = new double[n - 1];
            var beta = new double[n - 1];

            //������ ��� ��������
            alfa[0] = - top[0] / mid[0];
            beta[0] = right[0] / mid[0];
            for (int i = 1; i < n - 1; i++)
            {
                alfa[i] = -top[i] / (mid[i] + alfa[i - 1] * bot[i - 1]);
                beta[i] = (right[i] - bot[i - 1] * beta[i - 1]) / (mid[i] + alfa[i - 1] * bot[i - 1]);
            }

            //�������� ��� ��������
            res[n - 1] = (right[n - 1] - bot[n - 2] * beta[n - 2]) / (mid[n-1] + bot[n - 2] * alfa[n - 2]);
            for (int i = n - 2; i >= 0; i--)
            {
                res[i] = alfa[i] * res[i + 1] + beta[i];
            }

            return res;
        }
    }
}