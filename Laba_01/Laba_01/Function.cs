using System;

namespace Laba_01
{
    public delegate double MathFunc(double x);

    public abstract class Function
    {
        private MathFunc _f;

        public Function()
        {
            _f = null;
        }

        public Function(MathFunc f)
        {
          _f = f;
        }

        public double LeftBound
        {
            get; set;
        }

        public double RightBound
        {
            get; set;
        }

        public double Of(double x)
        {
            if(_f != null)
            {
                if(x >= LeftBound && x <= RightBound)
                {
                 return _f(x);
                }
                else
                {
                    throw new ArgumentException(String.Format("Invalid argument: {0} doesnt contained in [{1},{2}]",
                                                                x, LeftBound, RightBound));
                }
            }
            else
            {
                throw new NullReferenceException("Call function wasn't define");
            }
        }

        public abstract double RealDiff1(double x);
        public abstract double RealDiff2(double x);
    }
}