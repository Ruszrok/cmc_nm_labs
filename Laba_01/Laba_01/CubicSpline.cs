using System;
using System.Linq;

namespace Laba_01
{
   

    public class CubicSpline
    {
        private Function _sourceFunction;
        private SplineTurple[] _splines;
        private SplineError[] _errors;
        private int _gridSize;
        private double[] _gridNodes;

        public CubicSpline(Function f, int steps, double borderCondL = 0,
                                                  double borderCondR = 0)
        {
            _sourceFunction = f;
            _gridSize = steps + 1;
            _splines = new SplineTurple[_gridSize];
            _gridNodes = new double[_gridSize];

            double delta = Math.Abs(f.RightBound - f.LeftBound) / steps;

            for (int i = 0; i < _gridSize; ++i)
            {
                _gridNodes[i] = f.LeftBound + i*delta;
                _splines[i] = new SplineTurple();
                _splines[i].x = _gridNodes[i];
                _splines[i].a = f.Of(_gridNodes[i]);
                _splines[i].n = i;
            }
       
            double[] A = new double[steps-1];
            double[] B = new double[steps-1];
            double[] C = new double[steps-1];
            double[] F = new double[steps-1];

            for (int i = 0; i < steps - 1; i++)
            {
                A[i] = delta;
                B[i] = delta;
                C[i] = 4 * delta;

                F[i] = 6 * (Diff(f, i + 1, i+2) - Diff(f, i, i+1));
            }
            F[0] -= delta * borderCondL;
            F[steps - 2] -= delta * borderCondR;

            var c = NumericOperation.RibbonSolve(B, C, A, F);
            
            for (int i = 0; i < steps - 1; i++)
            {
                _splines[i+1].c = c[i];
            }
            _splines[0].c = borderCondL;
            _splines[steps].c = borderCondR;

            for (int i = 0; i < steps; ++i)
            {
                _splines[i+1].d = (_splines[i+1].c - _splines[i].c) / delta;
                _splines[i+1].b = delta * (2 * _splines[i+1].c + _splines[i].c) / 6 + (_splines[i + 1].a - _splines[i].a) / delta;
            }

            RecountErrors();
        }

        public SplineTurple[] Turples 
        { 
            get 
            { 
                return _splines.Where(x => x != _splines[0]).ToArray(); 
            } 
        }

        public SplineError[] Errors
        {
            get
            {
                return _errors;
            }
        }

        public double MaxFuncError { get { return _errors.Max(x => x.FuncErr); } }
        public double MaxDiffError { get { return _errors.Max(x => x.DiffErr); } }

        public double Of(double x)
        {
            SplineTurple neededSpline = FindSplineFor(x);

            return neededSpline.Of(x);
        }

        public double RealDiff1(double x)
        {
            SplineTurple neededSpline = FindSplineFor(x);

            return neededSpline.Diff1(x);
        }

        public double RealDiff2(double x)
        {
            SplineTurple neededSpline = FindSplineFor(x);

            return neededSpline.Diff2(x);
        }

        private SplineTurple FindSplineFor(double x)
        {
            SplineTurple neededSpline = null;
            if (x >= _splines[0].x && x <= _splines[_gridSize - 1].x)
            {
                neededSpline = BinarySearchTurpleFor(x);
            }
            else if (x < _splines[1].x)
            {
                neededSpline = _splines[1];
            }
            else
            {
                neededSpline = _splines[_gridSize - 1];
            }
            return neededSpline;
        }

        private SplineTurple BinarySearchTurpleFor(double x)
        {
            int left = 0;
            int right = _gridSize;

            while (left+1 < right)
            {
                int mid = left + (right - left) / 2;
                if (x < _splines[mid].x)
                {
                    right = mid;
                }
                else if (x >= _splines[mid].x)
                {
                    left = mid;
                }
            }

            return _splines[right];
        }

        private double Diff(Function f, int left, int right)
        {
            return (f.Of(_gridNodes[right]) - f.Of(_gridNodes[left])) / (_gridNodes[right] - _gridNodes[left]);
        }
        
        private void RecountErrors()
        {
            _errors = new SplineError[_gridSize - 1];
            
            double delta = Math.Abs(_gridNodes[1] - _gridNodes[0]) / _gridSize;
            
            for (int i = 0; i < _gridSize - 1; i++)
            {
                _errors[i] = new SplineError();
                _errors[i].n = i + 1;
                _errors[i].Xn = _gridNodes[i];
                _errors[i].Xn1 = _gridNodes[i+1];
                _errors[i].FuncErr = Double.MinValue;
                _errors[i].DiffErr = Double.MinValue;



                for (double current = _gridNodes[i]; current < _gridNodes[i + 1]; current += delta)
                {
                    _errors[i].FuncErr = Math.Max(_errors[i].FuncErr,
                                                    Math.Abs(_sourceFunction.Of(current) - Of(current)));
                    _errors[i].DiffErr = Math.Max(_errors[i].DiffErr,
                                                    Math.Abs(_sourceFunction.RealDiff1(current) - RealDiff1(current)));

                }

            }
        }
    }
}