﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Laba_01;

namespace Laba_01
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private ObservableDataSource<Point> functionDataPoints = new ObservableDataSource<Point>();
    private ObservableDataSource<Point> splineDataPoints = new ObservableDataSource<Point>();

    private Function currentFunction;
    private CubicSpline currentSpline;

    private LineGraph funcGraph;
    private LineGraph splineGraph;

    public MainWindow()
    {
      InitializeComponent();
    }

    private void goButton_Click(object sender, RoutedEventArgs e)
    {
        currentFunction = GetFunctionInstance();

        double leftCondition = txtLeftBorder.IsEnabled ? double.Parse(txtLeftBorder.Text):
                                                        currentFunction.RealDiff2(currentFunction.LeftBound);
        double rightCondition = txtRightBorder.IsEnabled ? double.Parse(txtRightBorder.Text) :
                                                        currentFunction.RealDiff2(currentFunction.RightBound);
        int stepCount = int.Parse(txtStepCount.Text);

        currentSpline = new CubicSpline(currentFunction, stepCount, leftCondition, rightCondition);

        if (rbFunc.IsChecked == true)
        {
            DrawFuncGraff();
        }
        else if (rbDiff1.IsChecked == true)
        {
            DrawDiff1Graff();
        }
        else
        {
            DrawDiff2Graff();
        }

        grdSplineTurples.ItemsSource = currentSpline.Turples;
        grdSplineErr.ItemsSource = currentSpline.Errors;
        this.DataContext = currentSpline;
    }

    private void DrawFuncGraff()
    {
        ClearPlotter();

        double dx = 0.01;
        double current = currentFunction.LeftBound;

        var l1 = new List<Point>();
        var l2 = new List<Point>();

        while (current <= currentFunction.RightBound)
        {
            l1.Add(new Point(current, currentFunction.Of(current)));
            l2.Add(new Point(current, currentSpline.Of(current)));
            current += dx;
        }

        functionDataPoints.AppendMany(l1);
        splineDataPoints.AppendMany(l2);
    }

    private void DrawDiff1Graff()
    {
        ClearPlotter();

        double dx = 0.01;
        double current = currentFunction.LeftBound;

        var l1 = new List<Point>();
        var l2 = new List<Point>();

        while (current <= currentFunction.RightBound)
        {
            l1.Add(new Point(current, currentFunction.RealDiff1(current)));
            l2.Add(new Point(current, currentSpline.RealDiff1(current)));
            current += dx;
        }

        functionDataPoints.AppendMany(l1);
        splineDataPoints.AppendMany(l2);
    }

    private void DrawDiff2Graff()
    {
        ClearPlotter();

        double dx = 0.01;
        double current = currentFunction.LeftBound;

        var l1 = new List<Point>();
        var l2 = new List<Point>();

        while (current <= currentFunction.RightBound)
        {
            l1.Add(new Point(current, currentFunction.RealDiff2(current)));
            l2.Add(new Point(current, currentSpline.RealDiff2(current)));
            current += dx;
        }

        functionDataPoints.AppendMany(l1);
        splineDataPoints.AppendMany(l2);
    }

    private void ClearPlotter()
    {
        if (funcGraph != null)
            plotter.Children.Remove(funcGraph);
        if (splineGraph != null)
            plotter.Children.Remove(splineGraph);

        functionDataPoints = new ObservableDataSource<Point>();
        splineDataPoints = new ObservableDataSource<Point>();
        
        funcGraph = plotter.AddLineGraph(functionDataPoints, Colors.Red, 2.0, ((ComboBoxItem)cmbFunction.SelectedItem).Content.ToString());
        splineGraph = plotter.AddLineGraph(splineDataPoints, Colors.Blue, 1.0, "spline");
    }
    
    private Function GetFunctionInstance()
    {
        if (cmbFunction.SelectedIndex == 0)
            return new TestFunction();
        if (cmbFunction.SelectedIndex == 1)
            return new MyFunction();
        if (cmbFunction.SelectedIndex == 2)
            return new MyFunctionCos10x();
        if (cmbFunction.SelectedIndex == 3)
            return new MyFunctionCos100x();

        return null;
    }

    private void rbFunc_Checked(object sender, RoutedEventArgs e)
    {
        if(currentFunction != null && currentSpline != null)
            DrawFuncGraff();
    }

    private void rbDiff1_Checked(object sender, RoutedEventArgs e)
    {
        if (currentFunction != null && currentSpline != null)
            DrawDiff1Graff();
    }

    private void rbDiff2_Checked(object sender, RoutedEventArgs e)
    {
        if (currentFunction != null && currentSpline != null)
            DrawDiff2Graff();
    }

    private void chkLeftBorder_Checked(object sender, RoutedEventArgs e)
    {
        txtLeftBorder.IsEnabled = false;
    }

    private void chkRightBorder_Checked(object sender, RoutedEventArgs e)
    {
        txtRightBorder.IsEnabled = false;
    }

    private void chkLeftBorder_Unchecked(object sender, RoutedEventArgs e)
    {
        txtLeftBorder.IsEnabled = true;
    }

    private void chkRightBorder_Unchecked(object sender, RoutedEventArgs e)
    {
        txtRightBorder.IsEnabled = true;
    }
  }
}
